import React, { Component } from 'react'
import { Row, Col } from "reactstrap"
/*#BeginStyle*/
const iconSosam = {
    height: 100,
    width: 100
};

const TitleDevis = {
    fontSize: 12,
    color: "red",
    marginTop: 9
}

const input = {
    backgroundcolor: "transparent",
    verticalalign: "middle",
    border: "transparent",
    outline: "none",
    marginLeft: 16,
    fontSize: 12
}
const TitleDate = {
    fontSize: 12,
    marginTop: 9
}

const SubTitle = {
    fontSize: 12
}

const SubTitleContact = {
    fontSize: 12,
    fontWeight: "bold"
}
/*End Style*/

class OBHeader extends Component {
    render() {
        return (
            <div className="container">
                <Row>
                    <Col md="6">
                        <img style={iconSosam} src={require("../images/sosam.jpg")} />
                        <h6 style={SubTitle}>12 place du cadran solaire</h6>
                        <h6 style={SubTitle}>77127 LIEUSAINT</h6>
                        <h6 style={SubTitle}>Tél: 09 77 21 54 546</h6>
                        <div style={{ marginTop: 30 }}>
                            <h6 style={SubTitle}>Votre contact:<span style={SubTitleContact}> Yohann</span></h6>
                            <h6 style={SubTitle}>Téléphone: <span style={SubTitleContact}>06 41 73 75 55</span> </h6>
                            <h6 style={SubTitle}>Mail:<span style={{ color: "#33b5e5" }}> yohann@sosam.fr</span> </h6>
                        </div>
                    </Col>




                    <Col md="6">
                        <div style={{ width: "100%", overflow: "auto", marginTop: 35, marginLeft: 24 }}>
                            <div style={{ float: "left" }}> <h6 style={TitleDevis}>Devis N°</h6> </div>
                            <div style={{ float: "left" }}> <p><input style={input} placeholder=".........." type="text" /></p> </div>
                        </div>

                        <div style={{ marginLeft: 10, width: "100%", overflow: "auto" }}>
                            <div style={{ float: "left" }}> <h6 style={TitleDate}>Lieusaint, Le </h6> </div>
                            <div style={{ float: "left" }} > <input style={input} placeholder=".........." type="text" /></div>
                        </div>
                        <div style={{ marginTop: 20 }}>
                            <h6 style={TitleDate}>A l'attention de</h6>
                            <input style={input} placeholder=".........." type="text" />
                        </div>
                    </Col>

                </Row>
            </div >
        )
    }
}
export default OBHeader;