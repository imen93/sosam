import React, { Component } from 'react';
import { Row, Col } from "reactstrap";

const input = {
    backgroundcolor: "transparent",
    verticalalign: "middle",
    border: "transparent",
    outline: "none",
    fontSize: 12,
    marginLeft: 4
}
const title = {
    fontSize: 12,
    marginTop: 8
}

const iconSosam = {
    height: 80,
    width: 80
};
class OBFooter extends Component {
    render() {
        return (
            <div>
                <h6 style={{ fontSize: 14, marginTop: 32 }}>Pour la bonne tenue de notre dossier, nous vous prions de nous retourner le présent document avec la mention « lu et approuvé, bon pour
commande », daté et signé.</h6>

                <Row>
                    <Col md="9">
                        <div style={{ marginTop: 20 }}>
                            <div style={{ float: "left" }}> <h6 style={title}>Fait à</h6></div>
                            <div style={{ float: "left" }}>  <input style={input} placeholder="..........................................," type="text" /></div>

                            <div style={{ marginLeft: 12 }}>
                                <div style={{ float: "left" }}><h6 style={title}>le</h6></div>
                                <div style={{ float: "left" }}> <input style={input} placeholder=".........................................." type="text" /></div>
                            </div>

                        </div>
                    </Col>
                    <Col md="2">
                        <img style={iconSosam} src={require("../images/sosam.jpg")} />
                        <img style={iconSosam} src={require("../images/sosam.jpg")} />
                    </Col>

                </Row>

                <Row>
                    <Col md="2"></Col>
                    <Col>
                        <h6 style={title}>Signature précédée de « lu et approuvé, bon pour commande </h6>
                    </Col>
                </Row>
                <Row>
                    <Col md="2"></Col>
                    <Col md="3">
                        <input style={{ backgroundColor: "#E0E0E0", height: 50, marginLeft: 50, outline: "none" }} type="text" />

                    </Col>
                </Row>


                <Row style={{ justifyContent: "center", marginTop: 50 }}>
                    <h6 style={title}>Sous réserve de nos disponibilités au moment de votre commande</h6>

                </Row>

                <Row style={{ justifyContent: "center" }}>
                    <h6 style={{ fontSize: 14 }}>Association SoSam - Numero Siret : 531 224 954 00010 - 12 place du cadran solaire 77127 Lieusaint</h6>

                </Row>
            </div>
        )
    }
}
export default OBFooter;