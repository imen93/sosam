import React, { Component } from 'react'
import { Row, Col, Container } from 'reactstrap'
import "../css/Main.css"
import "../css/Textarea.css"
import "../css/Input.css"
import OBHeader from '../components/OBHeader'
import OBFooter from '../components/OBFooter'
class Main extends Component {

    render() {
        return (
            <Container>
                <OBHeader />
                    <Row>
                        <Col md="6 border-right border-white" className="row-dark"><div className="center"> <label class="label"> Designation </label></div></Col>
                        <Col md="2 border-right border-white" className="row-dark"><div className="center"><label class="label"> Véhicule </label></div></Col>
                        <Col md="1 border-right border-white" className="row-dark" ><div className="center"><label class="label"> Qté </label></div></Col>
                        <Col md="1 border-right border-white" className="row-dark" ><div className="center"><label class="label"> Prix unitaire </label></div></Col>
                        <Col md="2 border-right border-white" className="row-dark" ><div className="center"><label class="label"> Montant </label></div> </Col>
                    </Row>
                    <Row>
                        <Col md="6 border-right border-white" className="row-gray"><textarea className="textarea" type="text" /> </Col>
                        <Col md="2 border-right border-white"> <img src={require("../images/bus.jpg")} style={{ width: 100, height: 56 }} /></Col>
                        <Col md="1 border-right border-white" className="row-gray">  <input type="text" /></Col>
                        <Col md="1 border-right border-white" className="row-gray">  <input type="text" /></Col>
                        <Col md="2 border-right border-white" className="row-gray">  <input type="text" /></Col>
                    </Row>
                    <Row>
                        <Col md="6 border-right border-white" className="row-clear"><textarea className="textarea" type="text" /> </Col>
                        <Col md="2 border-right border-white"></Col>
                        <Col md="1 border-right border-white" className="row-clear">  <input type="text" /></Col>
                        <Col md="1 border-right border-white" className="row-clear">  <input type="text" /></Col>
                        <Col md="2 border-right border-white" className="row-clear"> <input type="text" /></Col>
                    </Row>
                    <Row>
                        <Col md="6 border-right border-white" className="row-gray"><textarea className="textarea" type="text" /> </Col>
                        <Col md="2 border-right border-white"></Col>
                        <Col md="1 border-right border-white" className="row-gray">  <input type="text" /></Col>
                        <Col md="1 border-right border-white" className="row-gray">  <input type="text" /></Col>
                        <Col md="2 border-right border-white" className="row-gray"> <input type="text" /></Col>
                    </Row>
                    <Row>
                        <Col md="6 border-right border-white" className="row-clear"><textarea className="textarea" type="text" /> </Col>
                        <Col md="2 border-right border-white"></Col>
                        <Col md="1 border-right border-white" className="row-clear">  <input type="text" /></Col>
                        <Col md="1 border-right border-white" className="row-clear">  <input type="text" /></Col>
                        <Col md="2 border-right border-white" className="row-clear"> <input type="text" /></Col>
                    </Row>
                    <Row>
                        <Col md="6 border-right border-white" className="row-gray"><textarea className="textarea" type="text" /> </Col>
                        <Col md="2 border-right border-white"></Col>
                        <Col md="1 border-right border-white" className="row-gray">  <input type="text" /></Col>
                        <Col md="1 border-right border-white" className="row-gray">  <input type="text" /></Col>
                        <Col md="2 border-right border-white" className="row-gray"> <input type="text" /></Col>
                    </Row>
                    <Row>
                        <Col md="6 border-right border-white" className="row-clear"><textarea className="textarea" type="text" /> </Col>
                        <Col md="2 border-right border-white"></Col>
                        <Col md="1 border-right border-white" className="row-clear">  <input type="text" /></Col>
                        <Col md="1 border-right border-white" className="row-clear">  <input type="text" /></Col>
                        <Col md="2 border-right border-white" className="row-clear"> <input type="text" /></Col>
                    </Row >
                    <Row>
                        <Col md="9"><span className="label-date">Sous réserve de disponibilité à la date de confirmation</span></Col>
                        <Col md="3" className="col-clear-date">  <input type="text" /></Col>
                    </Row>
                    <Row>
                        <Col md="9"></Col>
                        <Col md="1" className="col-clear-unit-price-ht">HT</Col>
                        <Col md="2" className="col-clear-amount-ht"> <input type="text" /></Col>
                    </Row>
                    <Row>
                        <Col md="9"></Col>
                        <Col md="1" className="col-clear-unit-price-tva">TVA</Col>
                        <Col md="2" className="col-clear-amount-tva">  <input type="text" /></Col>
                    </Row>

                    <Row>
                        <Col md="8"></Col>
                        <Col md="2" className="col-dark-budget"><label className="label-budget">BUDGET TOTAL TIC EN EURO</label></Col>
                        <Col md="2" className="col-clear-budget">  <input type="text" /></Col>
                    </Row>
                    <Row><Col md="12" className="col-empty"></Col></Row>
                    <Row>
                        <Col md="4" className="col-dark-modalite"><div className="center"><label class="label">Modalité et délai de règlement</label></div></Col>
                        <Col md="8" className="col-gray-modalite"><label className="label-black">Versement d’un acompte de 50% à l’acceptation du présent devis<br />Versement du solde une semaine, avant la prestation pour les véhicules. Versement du solde un mois, avant la prestation pour les
                     autocars <br />Vous pouvez, si vous le souhaitez, effectuer un règlement anticipé du "Total à régler » <br />Ce devis ne deviendra commande qu’après réception de votre confirmation écrite, accompagnée de votre acompte de 50% du montant
                     total, sous réserve de nos disponibilités au moment de votre commande.</label></Col>
                    </Row>
                    <Row>
                        <Col md="4" className="col-dark-mode"><div className="center"><label class="label"> Mode de règlement</label></div> </Col>
                        <Col md="8" className="col-clear-mode"><label className="label-black">Virement bancaire:<br />IBAN: FR7610278060020002047970169<br />Chèque : A l’ordre de Sosam assos
                    Espèce</label></Col>
                    </Row>
                <OBFooter />
            </Container >


        )
    }
}
export default Main