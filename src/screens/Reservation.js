import React, { Component } from 'react'
import { Row, Col, Container } from 'reactstrap'
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from 'react-select'
import '../css/Reservation.css'

//Ville de france
const searchOptions = {
    componentRestrictions: {
        country: ['fr']
    },
    types: ['address']
}
const options = [
    {
        value: '1',
        label: 'Minivan 6-8 places'
    }, {
        value: '2',
        label: 'Berline 3 places'
    }, {
        value: '3',
        label: 'Minibus 17 places'
    },
    {
        value: '4',
        label: 'Autocar 53-80 places'
    }
]

const optionsTypeTrajet = [
    {
        value: '1',
        label: 'Aller simple'
    }, {
        value: '2',
        label: 'Aller - Retour'
    }, {
        value: '3',
        label: 'Circuit sur plusieurs heures'
    }, {
        value: '4',
        label: 'Circuit sur plusieurs jours'
    }
]

class Reservation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addressStart: '',
            addressEnd: '',
            locationStart: [],
            locationEnd: [],
            startDate: new Date(),
            listStep: [],
            vehicleType: '',
            typeOfTravel: '',
            distance: '',
            nbrHours: 0,
            nbrDays: 0
        };

    }
    //Fonction permet de modifier le state vehicleType
    handleChangeVehicule = vehicleType => {
        this.setState({ vehicleType });
    };

    //Fonction permet de modifier le state typeOfTravel
    handleChangeTravel = typeOfTravel => {
        this.setState({ typeOfTravel });
    };

    //Fonction permet de modifier le state nbrHours
    handleChangeNbrHours = (e) => {
        this.setState({ nbrHours: e.target.value })
    };

    //Fonction permet de modifier le state nbrDays
    handleChangeNbrDays = (e) => {
        this.setState({ nbrDays: e.target.value })
    }

    //Fonction permet de modifier le state startDate
    handleChange = date => {
        this.setState({ startDate: date });
    };

    /*** Begin Adress Départ ***/
    handleChangeAdrStart = address => {
        this.setState({ addressStart: address });
    };
    handleSelectAdrStart = address => {
        this.setState({ addressStart: address });
        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then(latLng => {
                this.setState({
                    locationStart: [latLng.lat, latLng.lng]
                })
            })
            .catch(error => console.error('Error', error));

    };
    //Ville de Départ
    _renderAutoCompleteDepart = () => {
        return (
            <PlacesAutocomplete
                value={this.state.addressStart}
                onChange={this.handleChangeAdrStart}
                onSelect={this.handleSelectAdrStart}
                searchOptions={searchOptions}>
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <Row>
                        <Col md="4">
                            <input
                                {...getInputProps({ placeholder: 'Entrer la ville de départ', className: 'location-search-input', })} />
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active
                                        ? 'suggestion-item--active'
                                        : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                        ? {
                                            backgroundColor: '#fafafa',
                                            cursor: 'pointer'
                                        }
                                        : {
                                            backgroundColor: '#ffffff',
                                            cursor: 'pointer'
                                        };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </Col>
                    </Row>
                )}
            </PlacesAutocomplete>
        )
    }
    /*** End Adress Départ ***/


    /*** Begin Adress Destination ***/
    handleChangeAdrEnd = address => {
        this.setState({ addressEnd: address });
    };
    handleSelectAdrEnd = address => {
        this.setState({ addressEnd: address });
        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then(latLng => {
                this.setState({
                    locationEnd: [latLng.lat, latLng.lng]
                })
            })
            .catch(error => console.error('Error', error));

    };
    //Ville d’arrivée
    _renderAutoCompleteDestination = () => {
        return (
            <PlacesAutocomplete
                value={this.state.addressEnd}
                onChange={this.handleChangeAdrEnd}
                onSelect={this.handleSelectAdrEnd}
                searchOptions={searchOptions}>
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <Row>
                        <Col md="4">
                            <input
                                {...getInputProps({ placeholder: 'Entrer la ville arrivée', className: 'location-search-input', })} />
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active
                                        ? 'suggestion-item--active'
                                        : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                        ? {
                                            backgroundColor: '#fafafa',
                                            cursor: 'pointer'
                                        }
                                        : {
                                            backgroundColor: '#ffffff',
                                            cursor: 'pointer'
                                        };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </Col>
                    </Row>
                )}
            </PlacesAutocomplete>
        )
    }
    /*** End Adress Destination ***/


    /*** Begin adress step ***/
    handleChangeAdr = i => address => {
        let listStep = [...this.state.listStep]
        listStep[i] = address
        this.setState({ listStep })
    };
    handleSelectAdr = i => address => {
        let listStep = [...this.state.listStep]
        listStep[i] = address
        this.setState({ listStep })
    };
    handleDeleteStep = i => e => {
        e.preventDefault()
        let listStep = [
            ...this
                .state
                .listStep
                .slice(0, i),
            ...this
                .state
                .listStep
                .slice(i + 1)
        ]
        this.setState({ listStep })
    }
    addStep = e => {
        e.preventDefault()
        let listStep = this
            .state
            .listStep
            .concat([''])
        this.setState({ listStep })
    }
    //Ajouter une étape
    _renderAutoCompleteStep = () => {
        return (this.state.listStep.map((step, index) => (
            <Row key={index}>
                <PlacesAutocomplete
                    onChange={this.handleChangeAdr(index)}
                    onSelect={this.handleSelectAdr(index)}
                    value={step}
                    searchOptions={searchOptions}>
                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                        <Col md="4">
                            <div
                                style={{
                                    position: 'relative',
                                    marginBottom: 10
                                }}>
                                <input
                                    {...getInputProps({ placeholder: 'Entrer l\'adresse de l\'étape ...', className: 'location-search-input', })} />
                                <img src={require("../images/cancel.png")} style={{ width: 24, height: 24, position: 'absolute', right: 4, top: 7 }} onClick={this.handleDeleteStep(index)} />
                            </div>
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active
                                        ? 'suggestion-item--active'
                                        : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                        ? {
                                            backgroundColor: '#fafafa',
                                            cursor: 'pointer'
                                        }
                                        : {
                                            backgroundColor: '#ffffff',
                                            cursor: 'pointer'
                                        };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </Col>
                    )}
                </PlacesAutocomplete>
            </Row >
        )))
    }
    /*** End adress step ***/


    //Fonction qui permet de calculer la distance entre deux points en Km
    //Appeller la fonction getPrice
    getDistance = async () => {
        let service = new window
            .google
            .maps
            .DistanceMatrixService();
        service.getDistanceMatrix({
            origins: [this.state.addressStart],
            destinations: [this.state.addressEnd],
            travelMode: window.google.maps.TravelMode.DRIVING,
            avoidHighways: false,
            avoidTolls: false
        }, (response, status) => {
            if (status === "OK") {
                let dist = response.rows[0].elements[0].distance.text;
                let distValue = response.rows[0].elements[0].distance.value;
                this.setState({ distance: dist })
                let price = this.getPrice(this.state.vehicleType.label, this.state.typeOfTravel.label, distValue, this.state.nbrHours, this.state.nbrDays)
                alert("La distance= " + this.state.distance + "   ,et le prix= " + price);
                return response;
            } else {
                alert("Error: " + status);
            }
        });
    }


    //Fonction qui permet de calculer le price selon le type de véhicule, le type de trajet, la distance, le nbrHeures et le nbr de jours
    getPrice = (vehicleType, typeOfTravel, distance, nbHour, nbDay) => {
        if (typeOfTravel === "Aller simple") {
            return this.getOneWayPrice(distance, vehicleType);
        }
        if (typeOfTravel === "Aller - Retour") {
            return this.getRoundTripPrice(distance, vehicleType);
        }
        if (typeOfTravel === "Circuit sur plusieurs heures") {
            return this.getRentalPerHourPrice(nbHour, vehicleType);
        }
        if (typeOfTravel === "Circuit sur plusieurs jours") {
            return this.getRentalPerDayPrice(nbDay, vehicleType);
        }
    }

    getOneWayPrice = (distance, vehicleType) => {
        let price;
        if (vehicleType === "Berline 3 places") {
            price = 1.8 * distance;
            if (price < 55) {
                return 55;
            }
            return price;
        }

        if (vehicleType === "Minivan 6-8 places") {
            price = 1.8 * distance;
            if (price < 65) {
                return 65;
            }
            return price;
        }

        if (vehicleType === "Minibus 17 places") {
            price = 1.8 * distance;
            if (price < 170) {
                return 170;
            }
            return price;
        }

        if (vehicleType === 'Autocar 53-80 places') {
            price = 1.8 * distance;
            if (price < 190) {
                return 190;
            }
            return price;
        }
    }

    getRoundTripPrice = (distance, vehicleType) => {
        let price;
        if (vehicleType === "Berline 3 places") {
            price = 1.8 * distance;
            price = price * 2;
            if (price < 55) {
                return 55 * 2;
            }
            return price;
        }

        if (vehicleType === "Minivan 6-8 places") {
            price = 1.8 * distance;
            price = price * 2;
            if (price < 65) {
                return 65 * 2;
            }
            return price;
        }

        if (vehicleType === "Minibus 17 places") {
            price = 1.8 * distance;
            price = price * 2;
            if (price < 170) {
                return 170 * 2;
            }
            return price;
        }

        if (vehicleType === "Autocar 53-80 places") {
            price = 1.8 * distance;
            price = price * 2;
            if (price < 190) {
                return 190 * 2;
            }
            return price;
        }
    }

    getRentalPerHourPrice = (nbHour, vehicleType) => {
        let pricePerHour;
        let price;
        if (vehicleType === "Berline 3 places") {
            pricePerHour = 65;
            price = pricePerHour * nbHour;
            if (price <= 2225) {
                return 250;
            }
            return price;
        }

        if (vehicleType === "Minivan 6-8 places") {
            pricePerHour = 70;
            price = pricePerHour * nbHour;
            if (price <= 255) {
                return 250;
            }
            return price;
        }

        if (vehicleType === "Minibus 17 places") {
            pricePerHour = 90;
            price = pricePerHour * nbHour;
            if (price <= 500) {
                return 500;
            }
            return price;
        }

        if (vehicleType === "Autocar 53-80 places") {
            pricePerHour = 90;
            price = pricePerHour * nbHour;
            if (price <= 600) {
                return 600;
            }
            return price;
        }
    }

    getRentalPerDayPrice = (nbDay, vehicleType) => {
        let pricePerDay;
        let price;
        let accommodation = 130;
        if (vehicleType === "Berline 3 places") {
            pricePerDay = 590;
            price = pricePerDay * nbDay;
            if (price <= 590) {
                return 720;
            }
            accommodation = accommodation * nbDay;
            price = price + accommodation;
            return price;
        }

        if (vehicleType === "Minivan 6-8 places") {
            pricePerDay = 590;
            price = pricePerDay * nbDay;
            if (price <= 590) {
                return 720;
            }
            accommodation = accommodation * nbDay;
            price = price + accommodation;
            return price;
        }

        if (vehicleType === "Minibus 17 places") {
            pricePerDay = 1490;
            price = pricePerDay * nbDay;
            if (price <= 1490) {
                return 990;
            }
            accommodation = accommodation * nbDay;
            price = price + accommodation;
            return price;
        }

        if (vehicleType === "Autocar 53-80 places") {
            pricePerDay = 1490;
            price = pricePerDay * nbDay;
            if (price <= 1490) {
                return 990;
            }
            accommodation = accommodation * nbDay;
            price = price + accommodation;
            return price;
        }
    }

    render() {
        return (
            <Container style={{
                backgroundColor: "#efeeee"
            }}>
                <Row style={{
                    marginBottom: 30
                }}>
                    <Col>
                        <h6
                            style={{
                                color: "#7a7a7a"
                            }}>Ville de Départ :</h6>
                        {this._renderAutoCompleteDepart()}
                    </Col>
                </Row>
                <div style={{
                    marginBottom: 10
                }}>
                    {this._renderAutoCompleteStep()}
                </div>
                <Row style={{
                    marginBottom: 30
                }}>
                    <Col>
                        <button onClick={this.addStep}>Ajouter une étape</button>
                    </Col>
                </Row>

                <Row style={{
                    marginBottom: 30
                }}>
                    <Col>
                        <h6
                            style={{
                                color: "#7a7a7a"
                            }}>Ville d'arrivée :</h6>
                        {this._renderAutoCompleteDestination()}
                    </Col>
                </Row>

                <Row style={{
                    marginBottom: 30
                }}>
                    <Col md="4">
                        <h6
                            style={{
                                color: "#7a7a7a"
                            }}>Date de départ :</h6>
                        <DatePicker
                            selected={this.state.startDate}
                            onChange={this.handleChange}
                            className='inputDate'></DatePicker>
                    </Col>
                </Row>

                <Row style={{
                    marginBottom: 30
                }}>
                    <Col md="4">
                        <h6
                            style={{
                                color: "#7a7a7a"
                            }}>Type de véhicule :
                        </h6>
                        <Select
                            options={options}
                            value={this.state.vehicleType}
                            onChange={this.handleChangeVehicule} />
                    </Col>

                </Row>

                <Row style={{
                    marginBottom: 30
                }}>
                    <Col md="4">
                        <h6
                            style={{
                                color: "#7a7a7a"
                            }}>Votre nom (indispensable) :</h6>
                        <input className='inputText' type="text" placeholder="Votre nom *"></input>
                    </Col>
                </Row>

                <Row style={{
                    marginBottom: 30
                }}>
                    <Col md="4">
                        <h6
                            style={{
                                color: "#7a7a7a"
                            }}>Votre email (indispensable) :</h6>
                        <input className='inputText' type="text" placeholder="Email *"></input>
                    </Col>
                </Row>

                <Row style={{
                    marginBottom: 30
                }}>
                    <Col md="4">
                        <h6
                            style={{
                                color: "#7a7a7a"
                            }}>Type de trajet :</h6>
                        <Select
                            options={optionsTypeTrajet}
                            onChange={this.handleChangeTravel}
                            value={this.state.typeOfTravel}
                            placeholder="---" />
                    </Col>
                </Row>

                {this.state.typeOfTravel.label === "Circuit sur plusieurs heures" ?
                    (
                        <Row style={{ marginBottom: 30 }}>
                            <Col md="4">
                                <h6>Le nombre d'heures (indispensable) :</h6>
                                <input className='inputText' min="0" type="number" onChange={this.handleChangeNbrHours}></input>
                            </Col>
                        </Row>
                    ) :
                    (
                        this.state.typeOfTravel.label === "Circuit sur plusieurs jours" ?
                            <Row style={{ marginBottom: 30 }}> <Col md="4">
                                <h6>Le nombre de jours (indispensable) :</h6>
                                <input className='inputText' min="0" type="number" onChange={this.handleChangeNbrDays}></input>
                            </Col></Row>
                            : null
                    )
                }

                <Row>
                    <Col md="1"></Col>
                    <Col>
                        <button type="button" className="btn btn-primary" onClick={this.getDistance}>Envoyer</button>
                    </Col>
                </Row>
            </Container >

        )
    }

}
export default Reservation;